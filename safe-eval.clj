(defn safe-eval [exp]
  (try
    (eval (read-string exp))
    (catch Exception e
      (prn "Oops... Undefined symbol")
      (Thread/sleep 1000)
      (-main))))